from flask import Flask
from flask import jsonify
from config import config
from tablas import db
from tablas import User,pais,cuenta_bancaria,usuario_tiene_moneda,moneda,precio_moneda
from flask import request

def create_app(enviroment):
	app = Flask(__name__)
	app.config.from_object(enviroment)
	with app.app_context():
		db.init_app(app)
		db.create_all()
	return app

# Accedemos a la clase config del archivo config.py
enviroment = config['development']
app = create_app(enviroment)

#----------------------------------------------CRUD USUARIO----------------------------------------------
# Endpoint Read usuarios
@app.route('/api/usuarios/', methods=['GET'])
def get_users():
	users = [ user.json() for user in User.query.all() ] 
	return jsonify({'users': users })
# Endpoint READ de cierto usuario con id <id>
@app.route('/api/usuarios/<id>', methods=['GET'])
def get_user(id):
	user = User.query.filter_by(id=id).first()
	if user is None:
		return jsonify({'message': 'El usuario no existe'}), 404
	return jsonify({'user': user.json() })

# Endpoint para insertar un usuario
@app.route('/api/usuarios/', methods=['POST'])
def create_user():
	json = request.get_json(force=True)

	if json.get('nombre') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	User.create(json['id'],json['nombre'],json['apellido'],json['correo'],json['pais'],json['contraseña'])
	users = [ user.json() for user in User.query.all() ] 
	return jsonify({'users': users })

# Endpoint para actualizar un usuario
@app.route('/api/usuarios/<id>', methods=['PUT'])
def update_user(id):
	user = User.query.filter_by(id=id).first()
	if user is None:
		return jsonify({'message': 'El usuario no existe'}), 404
	json = request.get_json(force=True)
	if json.get('nombre') is None:
		return jsonify({'message': 'Solicitud Incorrecta'}), 400
	user.nombre,user.apellido,user.contraseña,user.correo,user.pais= (json['nombre'],json['apellido'],json['contraseña'],json['correo'],json['pais'])
	user.update()
	return jsonify({'user': user.json() })

# Endpoint para eliminar el usuario con id igual a <id>
@app.route('/api/usuarios/<id>', methods=['DELETE'])
def delete_user(id):
	user = User.query.filter_by(id=id).first()
	if user is None:
		return jsonify({'message': 'El usuario no existe'}), 404

	user.delete()

	return jsonify({'user': user.json() })

# Endpoint obtener id mas alto
@app.route('/api/usuarios/id/', methods=['GET'])
def get_usuarioid():
	id = [dict(usuarios) for usuarios in User.get_usuarioid().fetchall()]
	
	return jsonify({'id': id })
#----------------------------------------------CRUD PAIS----------------------------------------------

# Endpoint Read paises
@app.route('/api/paises/', methods=['GET'])
def get_paises():
	paises = [ pais.json() for pais in pais.query.all() ] 
	return jsonify({'paises': paises })

# Endpoint READ de cierto pais con cod_pais <cod_pais>
@app.route('/api/paises/<cod_pais>', methods=['GET'])
def get_pais(cod_pais):
	paiss = pais.query.filter_by(cod_pais=cod_pais).first()
	if paiss is None:
		return jsonify({'message': 'El pais no existe'}), 404
	return jsonify({'pais': paiss.json() })

# Endpoint para insertar un pais
@app.route('/api/paises/', methods=['POST'])
def create_country():
	json = request.get_json(force=True)

	if json.get('nombre') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	pais.create(json['cod_pais'],json['nombre'])
	paises = [ pais.json() for pais in pais.query.all() ] 
	return jsonify({'paises': paises })

# Endpoint para actualizar un pais
@app.route('/api/paises/<cod_pais>', methods=['PUT'])
def update_pais(cod_pais):
	paiss = pais.query.filter_by(cod_pais=cod_pais).first()
	if paiss is None:
		return jsonify({'message': 'El pais no existe'}), 404
	json = request.get_json(force=True)
	if json.get('nombre') is None:
		return jsonify({'message': 'Solicitud Incorrecta'}), 400
	paiss.nombre= json['nombre']
	paiss.update()
	return jsonify({'pais': paiss.json() })

# Endpoint para eliminar el pais con cod_pais igual a <cod_pais>
@app.route('/api/paises/<cod_pais>', methods=['DELETE'])
def delete_pais(cod_pais):
	paiss = pais.query.filter_by(cod_pais=cod_pais).first()
	if paiss is None:
		return jsonify({'message': 'El Pais no existe'}), 404

	paiss.delete()

	return jsonify({'pais': paiss.json() })

# Endpoint obtener id mas alto
@app.route('/api/paises/id/', methods=['GET'])
def get_paisid():
	id = [dict(paises) for paises in pais.get_paiseid().fetchall()]
	
	return jsonify({'id': id })

#----------------------------------------------CRUD CUENTA_BANCARIA----------------------------------------------

# Endpoint Read cuenta_bancaria
@app.route('/api/cuenta_bancaria/', methods=['GET'])
def get_cuenta_bancaria():
	cuentas_bancarias = [ cuenta_bancaria.json() for cuenta_bancaria in cuenta_bancaria.query.all() ] 
	return jsonify({'cuenta_bancaria': cuentas_bancarias })

# Endpoint READ de cierta cuenta
@app.route('/api/cuenta_bancaria/<numero_cuenta>', methods=['GET'])
def get_cuenta(numero_cuenta):
	cuenta = cuenta_bancaria.query.filter_by(numero_cuenta=numero_cuenta).first()
	if cuenta is None:
		return jsonify({'message': 'La cuenta no existe'}), 404
	return jsonify({'cuenta_bancaria': cuenta.json() })

# Endpoint para insertar una cuenta
@app.route('/api/cuenta_bancaria/', methods=['POST'])
def create_user_account():
	json = request.get_json(force=True)

	if json.get('numero_cuenta') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	cuenta_bancaria.create(json['numero_cuenta'],json['id_usuario'],json['balance'])
	cuentas_bancarias = [ cuenta_bancaria.json() for cuenta_bancaria in cuenta_bancaria.query.all() ] 
	return jsonify({'cuenta_bancaria': cuentas_bancarias })

# Endpoint para actualizar una cuenta
@app.route('/api/cuenta_bancaria/<numero_cuenta>', methods=['PUT'])
def update_cuenta_bancaria(numero_cuenta):
	cuenta = cuenta_bancaria.query.filter_by(numero_cuenta=numero_cuenta).first()
	if cuenta is None:
		return jsonify({'message': 'La cuenta no existe'}), 404
	json = request.get_json(force=True)
	if json.get('id_usuario') is None:
		return jsonify({'message': 'Solicitud Incorrecta'}), 400
	cuenta.id_usuario,cuenta.balance= (json['id_usuario'],json['balance'])
	cuenta.update()
	return jsonify({'cuenta_bancaria': cuenta.json() })

# Endpoint para eliminar una cuenta
@app.route('/api/cuenta_bancaria/<numero_cuenta>', methods=['DELETE'])
def delete_cuenta(numero_cuenta):
	cuenta = cuenta_bancaria.query.filter_by(numero_cuenta=numero_cuenta).first()
	if cuenta is None:
		return jsonify({'message': 'El Pais no existe'}), 404

	cuenta.delete()

	return jsonify({'cuenta_bancaria': cuenta.json() })

# Endpoint obtener id mas alto
@app.route('/api/cuenta_bancaria/id/', methods=['GET'])
def get_cuentaid():
	id = [dict(cuenta) for cuenta in cuenta_bancaria.get_cuentaid().fetchall()]

	return jsonify({'id': id })
#----------------------------------------------CRUD USUARIO_TIENE_MONEDA----------------------------------------------

# Endpoint Read usuario_tiene_moneda
@app.route('/api/usuario_tiene_moneda/', methods=['GET'])
def get_usuario_tiene_monedas():
	usuarios_tienen_monedas = [ usuario_tiene_moneda.json() for usuario_tiene_moneda in usuario_tiene_moneda.query.all() ] 
	return jsonify({'usuario_tiene_moneda': usuarios_tienen_monedas })

# Endpoint READ las monedas de cierto usuario
@app.route('/api/usuario_tiene_moneda/<id_usuario>', methods=['GET'])
def get_usuario_tiene_moneda(id_usuario):
	usuario_tiene_monedas = [ usuario_tiene_moneda.json() for usuario_tiene_moneda in usuario_tiene_moneda.query.filter_by(id_usuario=id_usuario).all() ] 
	if usuario_tiene_monedas is None:
		return jsonify({'message': 'La cuenta no existe'}), 404
	return jsonify({'usuario_tiene_monedas': usuario_tiene_monedas})

# Endpoint para insertar una moneda a usuario_tiene_moneda
@app.route('/api/usuario_tiene_moneda/', methods=['POST'])
def create_user_coin():
	json = request.get_json(force=True)

	if json.get('id_moneda') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	usuario_tiene_moneda.create(json['id_moneda'],json['id_usuario'],json['balance'])
	usuarios_tienen_monedas = [ usuario_tiene_moneda.json() for usuario_tiene_moneda in usuario_tiene_moneda.query.all() ]  
	return jsonify({'usuario_tiene_moneda': usuarios_tienen_monedas })
    
# Endpoint para actualizar usuario tiene moneda
@app.route('/api/usuario_tiene_moneda/<id_usuario>/<id_moneda>', methods=['PUT'])
def update_user_money(id_usuario,id_moneda):
	user_money = usuario_tiene_moneda.query.filter_by(id_usuario=id_usuario).filter_by(id_moneda=id_moneda).first()
	if user_money is None:
		return jsonify({'message': 'El usuario no tiene la moneda'}), 404
	json = request.get_json(force=True)
	if json.get('balance') is None:
		return jsonify({'message': 'Solicitud Incorrecta'}), 400
	user_money.balance= (json['balance'])
	user_money.update()
	return jsonify({'usuario_tiene_moneda': user_money.json() })

# Endpoint para eliminar una moneda de cierto usuario
@app.route('/api/usuario_tiene_moneda/<id_usuario>/<id_moneda>', methods=['DELETE'])
def delete_user_money(id_usuario,id_moneda):
	user_money = usuario_tiene_moneda.query.filter_by(id_usuario=id_usuario).filter_by(id_moneda=id_moneda).first()
	if user_money is None:
		return jsonify({'message': 'El usuario no tiene la moneda'}), 404

	user_money.delete()

	return jsonify({'usuario_tiene_moneda': user_money.json() })

#----------------------------------------------CRUD MONEDA----------------------------------------------
# Endpoint Read moneda
@app.route('/api/moneda/', methods=['GET'])
def get_monedas():
	monedas = [ moneda.json() for moneda in moneda.query.all() ] 
	return jsonify({'moneda': monedas })

# Endpoint READ de cierta moneda
@app.route('/api/moneda/<id>', methods=['GET'])
def get_moneda(id):
	coin = moneda.query.filter_by(id=id).first()
	if coin is None:
		return jsonify({'message': 'La moneda no existe'}), 404
	return jsonify({'moneda': coin.json() })

# Endpoint para insertar una moneda
@app.route('/api/moneda/', methods=['POST'])
def create_coin():
	json = request.get_json(force=True)

	if json.get('nombre') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	moneda.create(json['id'],json['nombre'],json['sigla'])
	monedas = [ moneda.json() for moneda in moneda.query.all() ] 
	return jsonify({'moneda': monedas })

# Endpoint para actualizar una moneda
@app.route('/api/moneda/<id>', methods=['PUT'])
def update_money(id):
	money = moneda.query.filter_by(id=id).first()
	if money is None:
		return jsonify({'message': 'La moneda no existe'}), 404
	json = request.get_json(force=True)
	if json.get('nombre') is None:
		return jsonify({'message': 'Solicitud Incorrecta'}), 400
	money.nombre,money.sigla = (json['nombre'],json['sigla'])
	money.update()
	return jsonify({'moneda': money.json() })

# Endpoint para eliminar una moneda
@app.route('/api/moneda/<id>', methods=['DELETE'])
def delete_moneda(id):
	money = moneda.query.filter_by(id=id).first()
	if money is None:
		return jsonify({'message': 'La moneda no existe'}), 404

	money.delete()

	return jsonify({'moneda': money.json() })

# Endpoint obtener id mas alto
@app.route('/api/moneda/id/', methods=['GET'])
def get_monedaid():
	id = [dict(moneda) for moneda in moneda.get_monedaid().fetchall()]
	
	return jsonify({'id': id })
#----------------------------------------------CRUD PRECIO_MONEDA----------------------------------------------

# Endpoint Read precio_moneda
@app.route('/api/precio_moneda/', methods=['GET'])
def get_precio_moneda():
	precio_monedas = [ precio_moneda.json() for precio_moneda in precio_moneda.query.all() ] 
	return jsonify({'precio_moneda': precio_monedas })

# Endpoint para insertar un precio_moneda
@app.route('/api/precio_moneda/', methods=['POST'])
def create_price_coin():
	json = request.get_json(force=True)

	if json.get('id_moneda') is None:
		return jsonify({'message': 'El formato está mal'}), 400

	precio_moneda.create(json['id_moneda'],json['valor'])
	precio_monedas = [ precio_moneda.json() for precio_moneda in precio_moneda.query.all() ] 
	return jsonify({'precio_moneda': precio_monedas })

# Endpoint para actualizar el precio de moneda
@app.route('/api/precio_moneda/<id_moneda>/', methods=['PUT'])
def update_money_price(id_moneda):
    money_price = precio_moneda.query.filter_by(id_moneda=id_moneda).first()
    if money_price is None:
        return jsonify({'message': 'La moneda no existe'}), 404
    json = request.get_json(force=True)
    money_price = precio_moneda.query.filter_by(fecha=json['fecha']).first()
    if json.get('valor') is None:
        return jsonify({'message': 'Solicitud Incorrecta'}), 400
    money_price.valor = (json['valor'])
    money_price.update()
    return jsonify({'user': money_price.json() })

# Endpoint para eliminar un precio de moneda

@app.route('/api/precio_moneda/<id_moneda>/<fecha>/', methods=['DELETE'])
def delete_precio(id_moneda,fecha):
	fecha=fecha.replace("%20"," ")

	precio = precio_moneda.query.filter_by(id_moneda=id_moneda).filter_by(fecha=fecha).first()
	if precio is False:
		return jsonify({'message': 'registro incorrecto'}), 404
	precio.delete()

	return jsonify({'delete': precio.json() })

#----------------------------------------------CONSULTAS----------------------------------------------

#1. Obtener todos los usuarios registrados durante el año X
@app.route('/api/consultas/1/<year>', methods=['GET'])
def consulta_1(year):
	consulta=User.consulta_1(year=year)
	if consulta!=False:
		consulta1 = [dict(usuario) for usuario in consulta.fetchall()]
	else:
		return jsonify({'error': "error 500" })
	if len(consulta1)==0:
			return jsonify({'error': "error 404" })
	
	return jsonify({'consulta 1': consulta1 })

#2. Obtener todas las cuentas bancarias con un balance superior a X
@app.route('/api/consultas/2/<min>', methods=['GET'])
def consulta_2(min):
	consulta=cuenta_bancaria.consulta_2(min=min)
	if consulta!=False:
		consulta2 = [dict(cuenta) for cuenta in consulta.fetchall()]
	else:
		return jsonify({'error': "error 500" })

	if len(consulta2)==0:
			return jsonify({'error': "error 404" })

	for a in consulta2:
		a['balance']=float(a['balance'])

	return jsonify({'consulta 2': consulta2 })

#3. Obtener todos los usuarios que pertenecen al pais X
@app.route('/api/consultas/3/<country>', methods=['GET'])
def consulta_3(country):
	consulta=User.consulta_3(country=country)

	if consulta!=False:
		consulta3 = [dict(usuario) for usuario in consulta.fetchall()]
	else:
		return jsonify({'error': "error 500" })

	if len(consulta3)==0:
			return jsonify({'error': "error 404" })
	
	return jsonify({'consulta 3': consulta3 })

#4. Obtener el maximo valor historico de la moneda X
@app.route('/api/consultas/4/<moneda>', methods=['GET'])
def consulta_4(moneda):
	
	consulta=precio_moneda.consulta_4(coin=moneda)

	if consulta!=False:
		consulta4 = [dict(moneda) for moneda in consulta.fetchall()]
	else:
		return jsonify({'error': "error 500" })

	if len(consulta4)==0:
			return jsonify({'error': "error 404" })
	consulta4[0]['maximo']=float(consulta4[0]['maximo'])
	
	return jsonify({'consulta 4': consulta4 })

#5. Obtener la cantidad de moneda X en circulacion (Es decir, la suma de todas las cantidades de la moneda X que poseen todos los usuarios)
@app.route('/api/consultas/5/<moneda>', methods=['GET'])
def consulta_5(moneda):

	consulta=usuario_tiene_moneda.consulta_5(coin=moneda)

	if consulta!=False:
		consulta5 = [dict(moneda) for moneda in consulta.fetchall()]
	else:
		return jsonify({'error': "error 500" })

	if len(consulta5)==0:
			return jsonify({'error': "error 404" })
	for a in consulta5:
		a['total']=float(a['total'])
	
	return jsonify({'consulta 5': consulta5 })

#6. Obtener el TOP 3 de monedas mas populares, es decir, las que son poseidas por la mayor cantidad de usuarios diferentes
@app.route('/api/consultas/6', methods=['GET'])
def consulta_6():
	consulta6 = [dict(monedas) for monedas in usuario_tiene_moneda.consulta_6().fetchall()]
	if len(consulta6)==0:
			return jsonify({'error': "error 404" })
	return jsonify({'consulta 6': consulta6 })

#7. Obtener la moneda que mas cambio su valor durante el mes X
@app.route('/api/consultas/7/<month>/<year>', methods=['GET'])
def consulta_7(month,year):

	consulta=precio_moneda.consulta_7(month=month,year=year)

	if consulta!=False:
		consulta7 = [dict(moneda) for moneda in consulta.fetchall()]
	else:
		return jsonify({'error': "error 500" })

	if len(consulta7)==0:
			return jsonify({'error': "error 404" })
	return jsonify({'consulta 7': consulta7 })

#8. Obtener la criptomoneda mas abundante del usuario X
@app.route('/api/consultas/8/<id>', methods=['GET'])
def consulta_8(id):

	consulta=usuario_tiene_moneda.consulta_8(codigo=id)

	if consulta!=False:
		consulta8 = [dict(usuario) for usuario in consulta.fetchall()]
	else:
		return jsonify({'error': "error 500" })

	if len(consulta8)==0:
			return jsonify({'error': "error 404" })
	for a in consulta8:
		a['cantidad']=float(a['cantidad'])
	
	return jsonify({'consulta 8': consulta8 })

if __name__ == '__main__':
	app.run(debug=True)
