<?php

include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
$correo = $_SESSION["correo"];
/*Se crea tabla usuario para acceder a la información necesaria del usuario.
Esto se realiza mediante lógica SQL para acceder a los atributos de interés en la tabla usuario.*/
$tablaUsuarios =    "SELECT usuario.id,usuario.nombre as nombre,usuario.apellido,usuario.correo,
                          usuario.fecha_registro, pais.nombre as pais FROM usuario
                    INNER JOIN pais 
                    ON usuario.pais = pais.cod_pais";
/* Se crean arreglos para ir guardando en orden la información de los usuarios*/
$nombres = array();
$apellidos = array();
$correos = array();
$id = 0;

$rs = pg_query( $dbconn, $tablaUsuarios );
    if( $rs )
        {
             if( pg_num_rows($rs) > 0 )
            {
                // Se van almacenando los valores de cada usuario en los arrays correspondientes:
                while( $obj = pg_fetch_object($rs) )
                {
                    $nombres[$obj->id] =  $obj->nombre;
                    $apellidos[$obj->id] =  $obj->apellido;
                    $correos[$obj->id] =  $obj->correo;
                }
            }
        }

for ($i=1; $i <pg_num_rows($rs) ; $i++) 
{ 
    if($correos[$i] == $correo )
    {
        $id = $i;
        break;
    }
}
/* Se crean arreglos para ir guardando la información de las monedas que posee el usuario*/

$siglas =  array();
$nombreMoneda = array();
$cantidad = array();
$idMonedas = array();

/*Se crea tabla billetera para acceder a la información de las monedas que tiene cierto usuario.
Esto se realiza mediante lógica SQL para acceder a los atributos de interés en las tablas usuario y moneda.*/

$tablaBilletera = "SELECT usuario.nombre as nombre, usuario.apellido as apellido, moneda.nombre as nombre_moneda, 
                    usuario_tiene_moneda.balance as cantidad, moneda.sigla as sigla, usuario_tiene_moneda.id_moneda as id_mon
                    FROM usuario_tiene_moneda
                    INNER JOIN usuario
                    ON usuario_tiene_moneda.id_usuario = usuario.id
                    INNER JOIN moneda
                    ON usuario_tiene_moneda.id_moneda = moneda.id 
                    WHERE usuario.nombre = '$nombres[$id]' and usuario.apellido = '$apellidos[$id]'";

$rx = pg_query( $dbconn, $tablaBilletera );
$i=1;
$cantidad_monedas = pg_num_rows($rx); 
if($rx)
{
    if( pg_num_rows($rx) > 0 )
    {
        /* Se van almacenando los valores la información de las monedas que posee el usuario*/
        while($obj1 = pg_fetch_object($rx))
        {
            $siglas[$i] = $obj1->sigla;
            $nombreMoneda[$i] = $obj1 ->nombre_moneda;
            $cantidad[$i] = $obj1 ->cantidad;
            $idMonedas[$i]= $obj1 ->id_mon;
            $i++;
        }
    }        

}

$tablaMonedas = array();
/*De acuerdo al id de la moneda se lee la tabla con su valor ordenado desde el mas nuevo al mas antiguo limitado 
    en 1 para que esta solo contenga el ultimo valor de la moneda con el id requerido*/
for ($i=1; $i <=$cantidad_monedas ; $i++) { 
    $tablaMonedas[$i] = "SELECT * FROM precio_moneda
    WHERE precio_moneda.id_moneda = $idMonedas[$i]
    ORDER BY precio_moneda.fecha DESC
    LIMIT 1";
}
/*Se crea una arreglo donde se leen las tablas y se guarda el valor de la moneda*/
$valorMoneda = array();
for ($i=1; $i <=$cantidad_monedas ; $i++) 
{ 
    $valorMoneda[$i]= pg_fetch_object(pg_query( $dbconn,$tablaMonedas[$i]))->valor;
}
$total = array();
/* el valor total será el valor de la moneda multiplicado por la cantidad*/
for ($i=1; $i <=$cantidad_monedas ; $i++) 
{
    $total[$i]=$valorMoneda[$i]*$cantidad[$i];
}
pg_close($dbconn);

?>