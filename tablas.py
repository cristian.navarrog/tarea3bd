from datetime import datetime
from re import A
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

#----------------------------------------------ENTIDAD USUARIO----------------------------------------------
class User(db.Model):
    __tablename__ = 'usuario'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), nullable=False)
    apellido = db.Column(db.String(50), nullable=False)
    correo = db.Column(db.String(50), nullable=False)
    contraseña = db.Column(db.String(50), nullable=False)
    pais = db.Column(db.Integer, db.ForeignKey('pais.cod_pais'), nullable=True)
    accounts = db.relationship('cuenta_bancaria', cascade="all,delete", backref="parent", lazy='dynamic')
    monedero = db.relationship('usuario_tiene_moneda', cascade="all,delete", backref="parents", lazy='dynamic')
    fecha_registro = db.Column(db.DateTime(), nullable=False, default=db.func.current_timestamp())

    @classmethod

    def create(cls,id,nombre,apellido,correo,pais,contraseña):
        # Instanciamos un nuevo usuario y lo guardamos en la bd
        user = User(id=id,nombre=nombre,apellido=apellido,correo=correo,pais=pais,contraseña=contraseña)
        return user.save()

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()

            return self
        except:
            return False
    
    def json(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
            'apellido': self.apellido,
            'correo': self.correo,
            'contraseña': self.contraseña,
            'fecha de registro': self.fecha_registro,
            'pais':self.pais
        }

    def update(self):
        self.save()

    def get_usuarioid():
        try:
            consulta="SELECT usuario.id FROM usuario ORDER BY usuario.id DESC LIMIT 1"
            result = db.session.execute(consulta)
            return result
        except:
            return False

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False

    def consulta_1(year):
        try:
            consulta="SELECT * FROM usuario WHERE usuario.fecha_registro >= 'year-01-01' and 'year-12-31'>=usuario.fecha_registro"
            consulta=consulta.replace("year",year)
            result = db.session.execute(consulta)
            return result
        except:
            return False

    def consulta_3(country):
        try:
            consulta="SELECT pais.nombre as pais,usuario.apellido,usuario.nombre FROM usuario INNER JOIN pais ON usuario.pais = pais.cod_pais WHERE pais.nombre='country'"
            consulta=consulta.replace("country",country)
            result = db.session.execute(consulta)
            return result
        except:
            return False


#----------------------------------------------ENTIDAD PAIS----------------------------------------------
class pais(db.Model):
    __tablename__ = 'pais'
    cod_pais = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(45), nullable=False)
    ciudadanos = db.relationship('User', cascade="all,delete", backref="parents", lazy='dynamic')
    
    @classmethod

    def create(cls,cod_pais,nombre):
        country = pais(cod_pais=cod_pais,nombre=nombre)
        return country.save()
    
    def save(self):
        try:
            db.session.add(self)
            db.session.commit()

            return self
        except:
            return False
    
    def json(self):
        return {
            'cod_pais': self.cod_pais,
            'nombre': self.nombre
        }
    
    def update(self):
        self.save()

    def get_paiseid():
        try:
            consulta="SELECT pais.cod_pais FROM pais ORDER BY pais.cod_pais DESC LIMIT 1"
            result = db.session.execute(consulta)
            return result
        except:
            return False

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False


#----------------------------------------------ENTIDAD CUENTA_BANCARIA----------------------------------------------
class cuenta_bancaria(db.Model):
    __tablename__ = 'cuenta_bancaria'
    numero_cuenta = db.Column(db.Integer, primary_key=True)
    id_usuario = db.Column(db.Integer, db.ForeignKey('usuario.id'), nullable=False)
    balance = db.Column(db.Numeric, nullable=False)

    @classmethod

    def create(cls,numero_cuenta,id_usuario,balance):
        new_account = cuenta_bancaria(numero_cuenta=numero_cuenta,id_usuario=id_usuario,balance=balance)
        return new_account.save()

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()

            return self
        except:
            return False
    
    def json(self):
        return {
            'numero_cuenta': self.numero_cuenta,
            'id_usuario': self.id_usuario,
            'balance': float(self.balance)
        }

    def update(self):
        self.save()

    def get_cuentaid():
        try:
            consulta="SELECT cuenta_bancaria.numero_cuenta FROM cuenta_bancaria ORDER BY cuenta_bancaria.numero_cuenta DESC LIMIT 1"
            result = db.session.execute(consulta)
            return result
        except:
            return False

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False

    def consulta_2(min):
        try:
            consulta='SELECT * FROM cuenta_bancaria WHERE cuenta_bancaria.balance > min'
            consulta=consulta.replace("min",min)
            result = db.session.execute(consulta)
            return result
        except:
            return False

#----------------------------------------------ENTIDAD USUARIO_TIENE_MONEDA----------------------------------------------
class usuario_tiene_moneda(db.Model):
    __tablename__ = 'usuario_tiene_moneda'
    id_usuario = db.Column(db.Integer, db.ForeignKey('usuario.id'), primary_key=True)
    id_moneda  = db.Column(db.Integer, db.ForeignKey('moneda.id' ), primary_key=True)
    balance = db.Column(db.Numeric, nullable=False)

    @classmethod

    def create(cls,id_moneda,id_usuario,balance):
        new_coin = usuario_tiene_moneda(id_moneda=id_moneda,id_usuario=id_usuario,balance=balance)
        return new_coin.save()

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()

            return self
        except:
            return False
    
    def json(self):
        return {
            'id_usuario': self.id_usuario,
            'id_moneda': self.id_moneda,
            'balance': float(self.balance)
        }
    def update(self):
        self.save()
        
    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False

    def consulta_5(coin):
        try:
            consulta="SELECT moneda.sigla as Moneda, SUM(usuario_tiene_moneda.balance) as Total FROM usuario_tiene_moneda INNER JOIN moneda ON usuario_tiene_moneda.id_moneda=moneda.id WHERE moneda.sigla = 'coin' GROUP BY moneda.sigla"
            consulta=consulta.replace("coin",coin)
            result = db.session.execute(consulta)
            return result
        except:
            return False

    def consulta_6():
        try:
            consulta="SELECT moneda.nombre, COUNT(moneda.nombre) as cantidad FROM usuario_tiene_moneda INNER JOIN moneda ON usuario_tiene_moneda.id_moneda=moneda.id GROUP BY moneda.nombre ORDER BY COUNT(moneda.nombre) DESC LIMIT 3"
            result = db.session.execute(consulta)
            return result
        except:
            return False

    def consulta_8(codigo):
        try:
            consulta="SELECT usuario.nombre as nombre,  usuario.apellido as apellido, moneda.nombre as moneda, usuario_tiene_moneda.balance as cantidad FROM usuario_tiene_moneda INNER JOIN usuario ON usuario_tiene_moneda.id_usuario = usuario.id INNER JOIN moneda ON usuario_tiene_moneda.id_moneda = moneda.id WHERE usuario.id=codigo ORDER BY usuario_tiene_moneda.balance DESC LIMIT 1"
            consulta=consulta.replace("codigo",codigo)
            result = db.session.execute(consulta)
            return result
        except:
            return False
            


#----------------------------------------------ENTIDAD MONEDA----------------------------------------------
class moneda(db.Model):
    __tablename__ = 'moneda'
    id = db.Column(db.Integer, primary_key=True)
    precios = db.relationship('precio_moneda', cascade="all,delete", backref="parent", lazy='dynamic')
    tienen = db.relationship('usuario_tiene_moneda', cascade="all,delete", backref="parent", lazy='dynamic')
    sigla = db.Column(db.String(10), nullable=False)
    nombre = db.Column(db.String(80), nullable=False)

    @classmethod

    def create(cls,id,nombre,sigla):
        coin = moneda(id=id,nombre=nombre,sigla=sigla)
        return coin.save()
        
    def save(self):
        try:
            db.session.add(self)
            db.session.commit()

            return self
        except:
            return False
    
    def json(self):
        return {
            'id': self.id,
            'sigla': self.sigla,
            'nombre': self.nombre
        }
    
    def update(self):
        self.save()

    def get_monedaid():
        try:
            consulta="SELECT moneda.id FROM moneda ORDER BY moneda.id DESC LIMIT 1"
            result = db.session.execute(consulta)
            return result
        except:
            return False
    
    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False

#----------------------------------------------ENTIDAD PRECIO_MONEDA----------------------------------------------
class precio_moneda(db.Model):
    __tablename__ = 'precio_moneda'
    id_moneda = db.Column(db.Integer, db.ForeignKey('moneda.id'), primary_key=True)
    fecha = db.Column(db.DateTime(),primary_key=True,default=db.func.current_timestamp())
    valor = db.Column(db.Numeric, nullable=False)

    @classmethod
    def create(cls,id_moneda,valor):
        price = precio_moneda(id_moneda=id_moneda,valor=valor)
        return price.save()

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()

            return self
        except:
            return False
    
    def json(self):
        return {
            'id_moneda': self.id_moneda,
            'fecha': self.fecha.strftime('%Y-%m-%d %H:%M:%S.%f'),
            'valor': float(self.valor)
        }
    def update(self):
        self.save()
    
    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False

    def consulta_4(coin):
        try:
            consulta="SELECT moneda.nombre,MAX(precio_moneda.valor) as maximo FROM precio_moneda INNER JOIN moneda ON precio_moneda.id_moneda=moneda.id WHERE moneda.nombre= 'coin' GROUP BY moneda.nombre"
            consulta=consulta.replace("coin",coin)
            result = db.session.execute(consulta)
            return result
        except:
            return False

    def consulta_7(month,year):
        month=int(month)
        year=int(year)
        try:
            if month==12:
                month2=1
                year2='0'+str(year+1)
            else:
                month2=month+1
                year2=year
            consulta="SELECT moneda.nombre as Moneda, cambios.cuenta as Veces FROM (SELECT precio_moneda.id_moneda, COUNT(precio_moneda.fecha) as cuenta FROM precio_moneda WHERE precio_moneda.fecha>= 'year1-month1-01' and precio_moneda.fecha< 'year2-month2-01' GROUP BY precio_moneda.id_moneda ORDER BY COUNT(precio_moneda.fecha) DESC LIMIT 1) as cambios INNER JOIN moneda ON moneda.id=cambios.id_moneda"
            consulta=consulta.replace("year1",str(year))
            consulta=consulta.replace("year2",str(year2))
            consulta=consulta.replace("month1",str(month))
            consulta=consulta.replace("month2",str(month2))
            result = db.session.execute(consulta)
            return result
        except:
            return False


        
