Ramo:           -INF 239
Profesor:       -Cecilia Reyes
integrantes:			
-Jorge Álvarez  			Rol:	201721065-6
-Juan Álvarez   			Rol:	201773016-1
-Cristian Navarro			Rol:	201721036-2
Grupo 04.
Asunto: 1er Avance Tarea 3

Para este primer avance se implementó el CRUD en la API y los métodos GET,POST,PUT y DELETE tienen las rutas listas. Para esto se 
aplico el modelo de la tarea 1 y se transformo en un modelo de clases en Python utilizando SQLAlchemy.
Explicación CRUD:
Para los GET, se despliega en la url correspondiente una vista de todos los atributos de la entidad a la que se le aplico el método.
En el caso de  los POST se verifica que se esten ingresando todos los campos necesarios para luego crear el registro en la entidad 
correspondiende. Para verificar el correcto funcionamiento se fue probando con el Software Postman el cual permite hacer 
consultas sobre la API.

A continuación se detalla un ejemplo de lo que se debe ingresar en el campo BODY para realizar demanera correcta los POST:

-POST usuarios:
URL:http://127.0.0.1:5000/api/usuarios/

Campo BODY:
	{
		"id":30,
    		"nombre":"PruebaNombre",
    		"apellido":"PruebaApellido",
    		"contraseña":"Contraseñaprueba",
    		"pais":3,
    		"correo":"cprueba@gmail.com"
    
	}

-------------------------------------------------------------------------------------------------------------------------------------

-POST paises:
URL:http://127.0.0.1:5000/api/paises/

Campo BODY:
	{
    		"cod_pais":11,
    		"nombre":"PruebaPais"
	}

-------------------------------------------------------------------------------------------------------------------------------------

-POST moneda:
URL:http://127.0.0.1:5000/api/moneda/

Campo BODY:
	{
    		"id":11,
    		"nombre":"NewCoin",
    		"sigla":"NC"
	}

-------------------------------------------------------------------------------------------------------------------------------------

-POST cuenta_bancaria:
URL:http://127.0.0.1:5000/api/cuenta_bancaria/

Campo BODY:
	{
    		"balance":222.2,
    		"id_usuario":30,
    		"numero_cuenta":38
	}

-------------------------------------------------------------------------------------------------------------------------------------

-POST usuario_tiene_moneda:
URL:http://127.0.0.1:5000/api/usuario_tiene_moneda/

Campo BODY:
	{
    		"id_moneda":11,
    		"balance":111.1,
    		"id_usuario":28
	}

-------------------------------------------------------------------------------------------------------------------------------------

-POST precio_moneda:
URL:http://127.0.0.1:5000/api/precio_moneda/

Campo BODY:
	{
    		"id_moneda":11,
    		"valor":111.1
	}

-------------------------------------------------------------------------------------------------------------------------------------

En el caso de PUT se implementa las instrucciones necesarias para según la entidad lograr la actualización de manera correcta y dado
que se desea actualizar información de un registro en particular, las URL van asociadas a un id

A continuación se detalla un ejemplo de lo que se debe ingresar en el campo BODY para realizar de manera correcta los PUT.

-PUT usuarios:
URL:http://127.0.0.1:5000/api/usuarios/2

Campo BODY:
	{
    		"nombre":"Pruebaput",
    		"apellido":"apellidoput",
    		"contraseña":"passput",
    		"pais":8,
    		"correo":"cprueba2@gmail.com"
	}

-------------------------------------------------------------------------------------------------------------------------------------

-PUT paises:
URL:http://127.0.0.1:5000/api/paises/2

Campo BODY:
	{
    		"nombre":"PruebaActName"
	}

-------------------------------------------------------------------------------------------------------------------------------------

-PUT moneda:
URL:http://127.0.0.1:5000/api/moneda/2

Campo BODY:
	{
    		"nombre":"Pruebamonedaput",
    		"sigla":"PMP"
	}

-------------------------------------------------------------------------------------------------------------------------------------

-PUT cuenta_bancaria:
URL:http://127.0.0.1:5000/api/cuenta_bancaria/5

Campo BODY:
	{
		"balance":95.95,
		"id_usuario":5
	}

-------------------------------------------------------------------------------------------------------------------------------------

-PUT usuario_tiene_moneda:
URL:http://127.0.0.1:5000/api/usuario_tiene_moneda/3/7

Campo BODY:
	{
    		"balance":500.55
	}

-------------------------------------------------------------------------------------------------------------------------------------

-PUT precio_moneda:
URL:http://127.0.0.1:5000/api/precio_moneda/1/

Campo BODY:
	{
    		"fecha":"2020-07-02 09:28:08.715511",
    		"valor":999.99
	}

-------------------------------------------------------------------------------------------------------------------------------------

Para implementar el DELETE se revisó cuidadosamente las foreign key indicando en los casos correspondientes que se permita la eliminación
de datos en cascada para que no queden rastros de atributos registros inexistentes y no producir errores. 

A continuación se detalla un ejemplo de lo que se debe ingresar para realizar demanera correcta los DELETE
(solo en el caso de Delete precio_moneda se utiliza el body):

-------------------------------------------------------------------------------------------------------------------------------------

-DELETE precio_moneda:
URL:http://127.0.0.1:5000/api/precio_moneda/1/

Campo BODY:
	{
    "fecha":"2020-07-02 09:28:08.715511"
	}

-------------------------------------------------------------------------------------------------------------------------------------

-DELETE usuario:
URL:http://127.0.0.1:5000/api/usuarios/1

Campo BODY:
	{
	}

-------------------------------------------------------------------------------------------------------------------------------------

-DELETE pais:
URL:http://127.0.0.1:5000/api/paises/1

Campo BODY:
	{
	}

-------------------------------------------------------------------------------------------------------------------------------------

-DELETE cuenta:
URL:http://127.0.0.1:5000/api/cuenta_bancaria/1

Campo BODY:
	{
	}

-------------------------------------------------------------------------------------------------------------------------------------

-DELETE moneda de usuario:
URL:http://127.0.0.1:5000/api/usuario_tiene_moneda/1/1

Campo BODY:
	{
	}

-------------------------------------------------------------------------------------------------------------------------------------
ALERTA
-DELETE moneda:
URL:http://127.0.0.1:5000/api/moneda/1

Campo BODY:
	{
	}

-------------------------------------------------------------------------------------------------------------------------------------
