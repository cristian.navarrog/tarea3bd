<?php

$sesion = curl_init();

curl_setopt($sesion, CURLOPT_URL, 'http://127.0.0.1:5000/api/paises/id/');
curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
$ret = curl_exec($sesion);
$resultado = json_decode($ret, true);
$id_alto=$resultado["id"];
$id_final=$id_alto[0];
$id=$id_final["cod_pais"]+1;

$body = [
    "cod_pais" => $id,
    "nombre" => $_POST['pais'],
];

$post = json_encode($body);


curl_setopt($sesion, CURLOPT_URL, 'http://127.0.0.1:5000/api/paises/');

curl_setopt($sesion, CURLOPT_POST, 1);
curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);

curl_setopt($sesion, CURLOPT_POSTFIELDS, $post);


curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
$remote_server_output = curl_exec ($sesion);

// cerramos la sesión cURL
curl_close ($sesion);
// hacemos lo que queramos con los datos recibidos
// por ejemplo, los mostramos
print_r($remote_server_output);

header("Location: http://localhost/simulation/tabla_paises/paises.html");

