<?php

$sesion = curl_init();

curl_setopt($sesion, CURLOPT_URL, 'http://127.0.0.1:5000/api/usuarios/');
curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
$ret = curl_exec($sesion);
if (curl_errno($sesion)) echo curl_error($sesion);

else $get_usuarios = json_decode($ret, true);
$usuarios=$get_usuarios['users'];

foreach ($usuarios as $usuario){
    echo  "<tr><td> ";
    echo $usuario["id"];
    echo "</td><td> ";
    echo $usuario["nombre"];
    echo "</td><td> ";
    echo $usuario["apellido"];
    echo "</td><td> ";
    echo $usuario["contraseña"];
    echo "</td><td> ";
    echo $usuario["pais"];
    echo "</td><td> ";
    echo $usuario["correo"];
    echo "</td>";
    
    //VER
    echo "<td><a href=/simulation/tabla_usuarios/usuario.html?id=";
    echo  $usuario["id"];
    echo " class='btn btn-warning'>VER <i class='fas fa-search'></i></a>";
    //EDITAR
    echo "<a href=/simulation/tabla_usuarios/update.html?id=";
    echo  $usuario["id"];
    echo " class='btn btn-primary'>EDITAR <i class='fas fa-search'></i></a>";
    //ELIMINAR
    echo "<a href=/simulation/tabla_usuarios/delete_usuario.php?id=";
    echo  $usuario["id"];
    echo " class='btn btn-warning'>ELIMINAR <i class='fas fa-search'></i></a>";


    echo "<td><tr>";
}

curl_close($sesion);