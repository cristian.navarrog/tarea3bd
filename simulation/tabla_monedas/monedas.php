<?php

$sesion = curl_init();

curl_setopt($sesion, CURLOPT_URL, 'http://127.0.0.1:5000/api/moneda/');
curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
$ret = curl_exec($sesion);
if (curl_errno($sesion)) echo curl_error($sesion);

else $get_monedas = json_decode($ret, true);
$monedas=$get_monedas['moneda'];

//var_dump($text); mostrar json

foreach ($monedas as $moneda){
    echo  "<tr><td> ";
    echo $moneda["id"];
    echo "</td><td> ";
    echo $moneda["sigla"];
    echo "</td><td> ";
    echo $moneda["nombre"];
    echo "</td>";
    
    //VER
    echo "<td><a href=/simulation/tabla_monedas/moneda.html?id=";
    echo  $moneda["id"];
    echo " class='btn btn-warning'>VER <i class='fas fa-search'></i></a>";
    //EDITAR
    echo "<a href=/simulation/tabla_monedas/update.html?id=";
    echo  $moneda["id"];
    echo " class='btn btn-primary'>EDITAR <i class='fas fa-search'></i></a>";
    //ELIMINAR
    echo "<a href=/simulation/tabla_monedas/delete_moneda.php?id=";
    echo  $moneda["id"];
    echo " class='btn btn-warning'>ELIMINAR <i class='fas fa-search'></i></a>";


    echo "<td><tr>";
}

curl_close($sesion);