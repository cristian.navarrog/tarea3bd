<?php

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://127.0.0.1:5000/api/cuenta_bancaria/id/');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$retorno = curl_exec($ch);
$resultado = json_decode($retorno, true);
$id_alto=$resultado["id"];
$id_final=$id_alto[0];
$id=$id_final["numero_cuenta"]+1;
curl_close ($ch);


$sesion = curl_init();

$body = [
    "balance" => $_POST['balance'],
    "id_usuario" => $_POST['id_usuario'],
    "numero_cuenta" => $id
];

$post = json_encode($body);

curl_setopt($sesion, CURLOPT_URL, 'http://127.0.0.1:5000/api/cuenta_bancaria/');

curl_setopt($sesion, CURLOPT_POST, 1);
curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);

curl_setopt($sesion, CURLOPT_POSTFIELDS, $post);


curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
$remote_server_output = curl_exec ($sesion);

// cerramos la sesión cURL
curl_close ($sesion);
// hacemos lo que queramos con los datos recibidos
// por ejemplo, los mostramos
print_r($remote_server_output);

header("Location: http://localhost/simulation/tabla_cuentas/cuentas.html");