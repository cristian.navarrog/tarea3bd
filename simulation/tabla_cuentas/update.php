<?php

$sesion = curl_init();

$body = [
    "balance" => $_POST['balance'],
    "id_usuario" => $_POST['id_usuario']
];

$post = json_encode($body);

$key_id=$_GET['id'];

$ruta='http://127.0.0.1:5000/api/cuenta_bancaria/'.$key_id;

curl_setopt($sesion, CURLOPT_URL, $ruta);

curl_setopt($sesion, CURLOPT_CUSTOMREQUEST, 'PUT');
curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);

curl_setopt($sesion, CURLOPT_POSTFIELDS, $post);


curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
$remote_server_output = curl_exec ($sesion);

// cerramos la sesión cURL
curl_close ($sesion);
// hacemos lo que queramos con los datos recibidos
// por ejemplo, los mostramos
print_r($remote_server_output);

header("Location: http://localhost/simulation/tabla_cuentas/cuentas.html");