<?php

$sesion = curl_init();

curl_setopt($sesion, CURLOPT_URL, 'http://127.0.0.1:5000/api/precio_moneda/');
curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
$ret = curl_exec($sesion);
if (curl_errno($sesion)) echo curl_error($sesion);

else $get_precios = json_decode($ret, true);
$precios=$get_precios['precio_moneda'];

//var_dump($text); mostrar json

//self.fecha.strftime('%Y-%m-%d %H:%M:%S.%F');

foreach ($precios as $precio){
    echo  "<tr><td> ";
    echo $precio["id_moneda"];
    echo "</td><td> ";
    echo $precio["fecha"];
    echo "</td><td> ";
    echo $precio["valor"];
    echo "</td>";
    
    //EDITAR
    echo "<td><a href=/simulation/tabla_precios/update.html?id=";
    echo  $precio["id_moneda"];
    echo "&fecha=";
    echo  str_replace(' ', '%20', $precio["fecha"]);
    echo " class='btn btn-primary'>EDITAR <i class='fas fa-search'></i></a>";
    //ELIMINAR
    echo "<a href=/simulation/tabla_precios/delete_precio.php?id=";
    echo  $precio["id_moneda"];
    echo "&fecha=";
    echo  str_replace(' ', '%20', $precio["fecha"]);
    echo " class='btn btn-warning'>ELIMINAR <i class='fas fa-search'></i></a>";

    echo "<td><tr>";
}


curl_close($sesion);