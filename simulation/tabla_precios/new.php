<?php

$sesion = curl_init();

$body = [
    "id_moneda" => $_POST['moneda'],
    "valor" => $_POST['valor'],
];

$post = json_encode($body);


curl_setopt($sesion, CURLOPT_URL, 'http://127.0.0.1:5000/api/precio_moneda/');

curl_setopt($sesion, CURLOPT_POST, 1);
curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);

curl_setopt($sesion, CURLOPT_POSTFIELDS, $post);


curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
$remote_server_output = curl_exec ($sesion);

curl_close ($sesion);

header("Location: http://localhost/simulation/tabla_precios/precios.html");

