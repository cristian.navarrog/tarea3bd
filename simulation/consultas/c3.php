<?php

$sesion = curl_init();
    
$url="http://127.0.0.1:5000/api/consultas/3".'/'.$_POST['pais'];

curl_setopt($sesion, CURLOPT_URL, $url);

curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);

$ret = curl_exec($sesion);

$get_consulta = json_decode($ret, true);
if (!isset($get_consulta["error"])){
    $usuarios=$get_consulta['consulta 3'];
} else{
    $usuarios=[];
};

if (!empty($usuarios)) {
    echo '<div class="container shadow-lg rounded m-auto p-5">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Pais</th>
            </tr>
        </thead>
        <tbody>';
    foreach ($usuarios as $usuario){
        echo  "<tr><td> ";
        echo $usuario["nombre"];
        echo "</td><td> ";
        echo $usuario["apellido"];
        echo "</td><td> ";
        echo $usuario["pais"];
        echo "</td>";
    }
    echo '    </tbody>
    </table>
    ';

} else{
    echo "<p><b> No hay registros </b></p>"; 
}

curl_close($sesion);

