<?php

$sesion = curl_init();
    
$url="http://127.0.0.1:5000/api/consultas/2".'/'.$_POST['min'];

curl_setopt($sesion, CURLOPT_URL, $url);

curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);

$ret = curl_exec($sesion);

$get_consulta = json_decode($ret, true);
if (!isset($get_consulta["error"])){
    $cuentas=$get_consulta['consulta 2'];
} else{
    $cuentas=[];
};

if (!empty($cuentas)) {
    echo '<div class="container shadow-lg rounded m-auto p-5">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID usuario</th>
                <th scope="col">N° Cuenta</th>
                <th scope="col">Balance</th>
            </tr>
        </thead>
        <tbody>';
    foreach ($cuentas as $cuenta){
        echo  "<tr><td> ";
        echo $cuenta["id_usuario"];
        echo "</td><td> ";
        echo $cuenta["numero_cuenta"];
        echo "</td><td> ";
        echo $cuenta["balance"];
        echo "</td>";
    }
    echo '    </tbody>
    </table>
    ';

} else{
    echo "<p><b> No hay registros </b></p>"; 
}

curl_close($sesion);
