<?php
/* Este archivo maneja la lógica de obtener los datos de los usuarios */
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';

$tablaUsuarios =    "SELECT usuario.id,usuario.nombre as nombre,usuario.apellido,usuario.correo,
                            usuario.contraseña, usuario.fecha_registro, pais.nombre as pais FROM usuario
                    INNER JOIN pais 
                    ON usuario.pais = pais.cod_pais";
#Se crean arrays para posterior llenado con la infor de los usuarios
$ids = array();
$nombres = array();
$apellidos = array();
$correos = array();
$fecha_registro = array();
$paises = array();
$contraseñas = array();

$rs = pg_query( $dbconn, $tablaUsuarios );
    if( $rs )
        {
             if( pg_num_rows($rs) > 0 )
            {
                // Se guarda en los arrays la información de los usuarios
                while( $obj = pg_fetch_object($rs) )
                {
                    $ids[$obj->id] =  $obj->id;
                    $nombres[$obj->id] =  $obj->nombre;
                    $apellidos[$obj->id] =  $obj->apellido;
                    $correos[$obj->id] =  $obj->correo;
                    $paises[$obj->id] = $obj->pais;
                    $fecha_registro[$obj->id] =  $obj->fecha_registro;
                    $contraseñas[$obj->id] = $obj->contraseña;
                }
            }
        }
pg_close($dbconn);
?>
