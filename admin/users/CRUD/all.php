<?php
/* Este archivo maneja la lógica de obtener los datos de todos los usuarios */
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
$tabla_usuarios = "SELECT id,nombre,apellido,correo FROM usuario";

$ids = array();
$nombres = array();
$apellidos = array();
$correos = array();
$rs = pg_query( $dbconn, $tabla_usuarios );
    if( $rs )
        {
             if( pg_num_rows($rs) > 0 )
            {
                // Recorrer el resource y mostrar los datos:
                while( $obj = pg_fetch_object($rs) )
                {
                    $ids[$obj->id] =  $obj->id;
                    $nombres[$obj->id] =  $obj->nombre;
                    $apellidos[$obj->id] =  $obj->apellido;
                    $correos[$obj->id] =  $obj->correo;
                }
            }
        }

$result_id = pg_query_params($dbconn, "SELECT Usuario.id FROM usuario ORDER BY id ASC", array());
$row_id = pg_fetch_assoc($result_id);
$first_id=$row_id['id'];

$result_id2 = pg_query_params($dbconn, "SELECT Usuario.id FROM usuario ORDER BY id DESC", array());
$row_id2 = pg_fetch_assoc($result_id2);
$last_id=$row_id2['id']+1;
pg_close($dbconn);
?>