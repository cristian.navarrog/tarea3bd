Ramo:           -INF 239
Profesor:       -Cecilia Reyes
integrantes:			
-Jorge Álvarez  			Rol:	201721065-6
-Juan Álvarez   			Rol:	201773016-1
-Cristian Navarro			Rol:	201721036-2
Grupo 04.
Asunto: Tarea 3 Final

Las consultas fueron hechas usando el metodo de consultas personalizadas descrito en el gitbook de base de datos.
Las querys corresponden con las de la tarea 1 a excepcion de algunas que fueron levemente modificadas.


-Consulta 1
URL:http://127.0.0.1:5000/api/consultas/1/<year>
Ejemplo:http://127.0.0.1:5000/api/consultas/1/2019

-------------------------------------------------------------------------------------------------------------------------------------

-Consulta 2
URL:http://127.0.0.1:5000/api/consultas/2/<min>
Ejemplo:http://127.0.0.1:5000/api/consultas/2/3000

-------------------------------------------------------------------------------------------------------------------------------------

-Consulta 3
URL:http://127.0.0.1:5000/api/consultas/3/<pais>
Ejemplo:http://127.0.0.1:5000/api/consultas/3/Angola

-------------------------------------------------------------------------------------------------------------------------------------

-Consulta 4
URL:http://127.0.0.1:5000/api/consultas/4/<moneda>
Ejemplo:http://127.0.0.1:5000/api/consultas/4/Bitcoin

-------------------------------------------------------------------------------------------------------------------------------------

-Consulta 5
URL:http://127.0.0.1:5000/api/consultas/5/<moneda>
Ejemplo:http://127.0.0.1:5000/api/consultas/5/BTC

-------------------------------------------------------------------------------------------------------------------------------------

-Consulta 6
URL:http://127.0.0.1:5000/api/consultas/6

-------------------------------------------------------------------------------------------------------------------------------------

-Consulta 7
URL:http://127.0.0.1:5000/api/consultas/7/<mes>/<año>
Ejemplo:http://127.0.0.1:5000/api/consultas/7/11/2020

-------------------------------------------------------------------------------------------------------------------------------------

-Consulta 8
URL:http://127.0.0.1:5000/api/consultas/8/<id>
URL:http://127.0.0.1:5000/api/consultas/8/3


Se actualizo delete precio_moneda para de modo que los datos se ingresan en la url
-------------------------------------------------------------------------------------------------------------------------------------

-DELETE precio_moneda:
URL:http://127.0.0.1:5000/api/precio_moneda/1/2020-07-02 09:28:08.715511

-------------------------------------------------------------------------------------------------------------------------------------

DESCRIPCION Y SUPUESTOS DE LA INTERFAZ

La interfaz de las consultas consiste en una pagina en html y un archivo php que realiza la consulta y muestra los resultados.
Se asume que se ingresaran los datos de manera correcta
En caso de no existir un retorno o de existir un error en la consulta aparece el mensaje "no hay registros"

La interfaz del CRUD muestra
Al momento de hacer un PUT se deben ingresar todos los datos solicitados
Se implemento un id de incremento automatico en el CREATE de pais, usuario, moneda y cuenta
Este incremento de id esta implementado solo dentro de la interfaz

