<?php
/* Este archivo contiene la lógica de crear un usuario desde el formulario de registro */
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
/*A continuación se leen los valores registrados por el usuario y se almacenan en distintas variables*/
date_default_timezone_set('America/Santiago');
$name=$_POST['nombre'];
$lastname=$_POST['apellido'];
$email=$_POST['correo'];
$pass=$_POST['password'];
$pass2=$_POST['password2'];
$country=$_POST['pais'];
$columnas="id,nombre,apellido,correo,contraseña,pais,fecha_registro";
$fecha = date('Y-m-d H:i:s');
$admin=0;

/*Si el usuario se registro de manera correcta ingresando 2 contraseñas iguales, se procesa el registro*/
if ($pass==$pass2){
    #EN las siguientes 3 lineas se obtiene el último id registrado y se incrementa en 1 para añadirlo al nuevo usuario
    $result_id = pg_query_params($dbconn, "SELECT Usuario.id FROM usuario ORDER BY id DESC", array());
    $row_id = pg_fetch_assoc($result_id);
    $new_id=$row_id['id']+1;
    $valores= "$new_id,'$name','$lastname','$email','$pass',$country,'$fecha',$admin";
    #Se realiza el INSERT en la tabla usuario con las datos que registro el usuario
    $registrar="INSERT INTO usuario(id,nombre,apellido,correo,contraseña,pais,fecha_registro,admin) VALUES($valores)";
    $sol_reg = pg_query($dbconn,$registrar);
    if($sol_reg){
        header('Location:../index.html');

    }else
        echo " No se pudo añadir el usuario";

    pg_close($dbconn);
}
/*Si el usuario se registro de manera incorrecta ingresando 2 contraseñas distintas, 
se redirige a la pagina principal sin hacer el registro*/
else{
    header('Location:../index.html');
}
?>